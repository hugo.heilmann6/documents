\input{preamble}

\begin{document}
	\includecourse

	\setcounter{section}{3}
	\section{Tautologies}%
	\label{sec:tautologies}

	\begin{demo}
		Montrons que non(P et Q) $\iff$ non(P) ou non (Q)

		\bigskip
		\begin{NiceTabular}[hvlines]{ccccccc}
			P & Q & P et Q & non(P et Q) & non P & non Q & non P ou non Q  \\
			V & V & V & F & F & F & F \\
			V & F & F & V & F & V & V \\
			F & V & F & V & V & F & V \\
			F & F & F & V & V & V & V \\
		\end{NiceTabular}

		\bigskip
		Montrons que (P et (P $\implies$ Q)) $\implies$ Q

		\bigskip
		\begin{NiceTabular}[hvlines]{ccccc}
			P & Q & p $\implies$ Q & P et $P \implies Q$ & (P et (P  $\implies$ Q))
			$\implies$ Q \\
			V & V & V & V & V \\
			V & F & F & F & V \\
			F & V & V & F & V \\
			F & F & V & F & V \\
		\end{NiceTabular}
	\end{demo}

	\begin{expls}
		Démontrons que $\forall x \in \R, \exists y \in \R, x \le y$

		Soit $x \in \R$

		On pose $y = x + 1$

		On a : $y \in \R$ et $y \ge x$

		On a montré que : $\forall x \in \R, \exists y \in \R, x \le y$

		\bigskip

		Démontrons que $\exists y \in \R, \forall x \in \R, x \le y$ (P) est fausse.

		La négation de cette assertion est : $\forall y \in \R, \exists x \in
		\R, x > y$

		Soit $y \in \R$.

		On pose $x = y + 1 $.

		On a : $x \in \R$ et $x > y$

		On a montré que non (P) est vraie, donc P est fausse.
	\end{expls}

	\setcounter{section}{5}
	\section{Principes de raisonnement}%
	\label{sec:principes_de_raisonnement}

	\subsection{Raisonnement direct}%
	\label{sub:raisonnement_direct}

	\begin{expl}
		Soit $n \in \N$. Montrons que si $n$ est pair, alors $n^2$ est pair.

		\bigskip

		Soit $n \in \N$.
		\begin{addmargin}[1em]{0pt}
			On suppose que $n$ est pair.

			On en déduit que $n$ s'écrit $n = 2k$ où $k \in \N$.

			D'où $n^2 = (2k)^2 = 4k^2 = 2(2k^2)$

			Or $2k^2 \in \N$, donc $n^2$ est pair.
		\end{addmargin}
	\end{expl}

	\subsection{Raisonnement par contraposée}%
	\label{sub:raisonnement_par_contraposee}

	\begin{expl}
		Soit $n \in \N$. Montrer que si $n^2$ est pair alors  $n$ est pair.

		Raisonnons par contraposée.

		Soit $n \in \N$.

		\begin{addmargin}[1em]{0pt}
			On suppose que $n$ est impair.

			$n$ s'écrit donc $n = 2k + 1$ où $k \in \N$.

			$n^2$ s'écrit donc
			\[
				n^2 = (2k+1)^2 = 4k^2 + 4k + 1 = 2(2k^2 + 2k) + 1
		\]

		Or $2k^2 + 2k \in \N$, donc $n^2$ est impair.
		\end{addmargin}

		\bigskip
		On a donc montré que si  $n^2$ est pair alors $n$ est impair.
	\end{expl}

	\subsection{Raisonnement par l'absurde}%
	\label{sub:raisonnement_par_l_absurde}

	\begin{expl}
		Montrons que $\sqrt{2}$ est irrationnel.

		Raisonnons par l'absurde.

		Supposons que $\sqrt{2}$ soit rationnel.

		$\sqrt{2}$ s'écrit donc $\frac{p}{q}$ où $p$ et $q$ sont deux entiers naturels non
		nuls premiers entre eux.

		On en déduit que $2 = \frac{p^2}{q^2}$

		D'où $p^2 = 2q^2$ (*)

		$q^2$ étant un entier naturel, on en déduit que $p^2$ est pair.

		Donc $p$ est également pair.

		$p$ s'écrit donc $p = 2k$ où $k \in \N$.

		L'égalité (*) devient $4k^2 = 2q^2$.

		Donc $q^2 = 2k^2$.

		$k^2$ étant un entier naturel, on en déduit que $q^2$ est pair.

		Donc $q$ est également pair.

		\Finalement $p$ et $q$ sont pairs.

		Cela contredit le fait que $p$ et $q$ soient premiers entre eux.

		\Conclusion : $\sqrt{2}$ est irrationnel.
	\end{expl}

	\subsection{Raisonnement par disjonction de cas}%
	\label{sub:raisonnement_par_disjonction_de_cas}

	\begin{expl}
		Montrer que si $x \in \intervalleff{-2}{3}$ alors $x^2 \in \intervalleff{0}{9}$

		On suppose que $x \in \intervalleff{-2}{3}$.
		\begin{itemize}
			\item 1er cas : $x \in \intervalleff{-2}{0}$.
			\begin{align*}
				-2 \le x \le 0
				\intertext{La fonction carrée est décroissante sur
				$\intervalleof{-\infty}{0}$ :}
				4 \ge x^2 \ge 0
			\end{align*}
			C'est à dire que $x^2 \in \intervalleff{0}{4}$.

			\item 2e cas : $x \in \intervalleff{0}{3}$.

			\begin{align*}
				0 \le x \le 3
				\intertext{La fonction carrée est croissante sur
				$\intervalleof{0}{+\infty}$ :}
				0 \le  x^2 \le 9
			\end{align*}
			C'est à dire que $x^2 \in \intervalleff{0}{9}$.
		\end{itemize}

		\bigskip
		\Finalement $x^2 \in \intervalleff{0}{4} \ou x^2 \in \intervalleff{0}{9}$,

		donc $x^2 \in \intervalleff{0}{9}$.
	\end{expl}

	\subsection{Raisonnement par récurrence}%
	\label{sub:raisonnement_par_recurrence}

	\begin{expl}
		Montrer que :
		\[
			\forall n \in \N^{*}, \underbrace{\forall \theta \in \R,
			(e^{i\theta})^{n} = e^{i n\theta}}_{P_n}
		\]

		Montrons par récurrence que : $\forall n \in \N^{*}, P_n$

		\highlighty{Initialisation} : Montrons  $P_1$

		Soit  $\theta \in \R$.

		$(e^{i\theta})^{1} = e^{i\theta} = e^{i \times \theta \times 1}$

		Donc $P_1$.

		\bigskip
		\highlighty{Hérédité} : Soit $n \in \N^{*}$. Supposons $P_n$ et montrons
		$P_{n+1}$.

		Soit $\theta \in n\R$.

		\begin{align*}
			(e^{i \theta})^{n+1} &= (e^{i \theta})^{n} \times e^{i \theta} \\
			 &= e^{i n \theta} \times e^{i \theta} \text{(HR)} \\
			 &= e^{i(n \theta + \theta} \\
			 &= e^{i (n+1) \theta} \\
		\end{align*}

		Donc, $\forall \theta \in \R, \left( e^{i \theta} \right)^{n+1} = e^{i(n + 1)
		\theta}$

		C'est à dire que $P_{n+1}$ est prouvée.

		\Conclusion : $P_1$ et $P_{n+1}$ sont vraies, donc $P_n $.

		\subsection{Raisonnement par analyse-synthèse}%
		\label{sub:raisonnement_par_analyse_synthese}

		\begin{expl}
			Montrer qu'il existe $x \in \R$ tel que $\sqrt{x^2 + 8} = x + 2$.

			\highlighty{Analyse} : Supposons qu'il existe $x \in \R$ tel que
			$\sqrt{x^2 + 8} = x+2$.

			En élevant au carré chaque membre de l'égalité, on obtient $x^2 + 8 = x^2 + 4x
			+ 4$ donc $x = 1$.

			 \highlighty{Synthèse} :

			 Prenons $x = 1$.

			 D'une part : $\sqrt{1^2 + 8} = \sqrt{9} = 3$.

			 D'autre part : $1 + 2 = 3$.

			 \Finalement $\sqrt{1^2 + 8} = 1 + 2$.

			 \Conclusion : $\exists x \in \R, \sqrt{x^2 + 8} = x + 2$.
		\end{expl}
	\end{expl}
\end{document}
